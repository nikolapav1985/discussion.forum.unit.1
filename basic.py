#!/usr/bin/python

# Discussion forum unit 1 code

print 'Hello, World!'
print 1/2
print type(1/2)
print (01)
print 1/(2/3)

"""
My version of python 2.7.12. Current lines cover some basic examples. These include print a string, a number (evaluated to integer in this case 1/2), print a type of a number (integer again type(1/2)), print a number (integer again 01) and print a number (division by zero error in this case 1/(2/3)).

According to the guide a string is just an array of characters. Next integers can be signed (positive or negative) or unsigned (just positive) and usually have no decimal point. Integer should be equivalent to c long (64 bits on 64 bit system). My system is Lubuntu 16.04 LTS kernel version 4.13.0 (64 bit version).
"""
